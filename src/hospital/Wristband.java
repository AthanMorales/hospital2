/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public abstract class Wristband {
    private String name;
    private Doctor doc;
    private Date dob;
    private Barcode barCode;
    List<MedicalDetail> medicaments;

    protected Wristband(String name, Doctor doc, Date dob, Barcode barCode, List<MedicalDetail> medicaments) {
        this.name = name;
        this.doc = doc;
        this.dob = dob;
        this.barCode = barCode;
        this.medicaments = medicaments;
    }
    
    public abstract String toString();

    public String getName() {
        return name;
    }

    public Doctor getDoc() {
        return doc;
    }

    public Date getDob() {
        return dob;
    }

    public Barcode getBarCode() {
        return barCode;
    }

    public List<MedicalDetail> getMedicaments() {
        return medicaments;
    }
}
