/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Patient extends Person{
    
    List<Wristband> bands;

    public Patient(List<Wristband> bands, String name) {
        super(name);
        this.bands = bands;
    }

    public List<Wristband> getBands() {
        return bands;
    }
}
