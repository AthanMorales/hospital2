/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class HospitalSimulation {

    public static void main(String args[]) {
        Doctor doctor = new Doctor("Dr.Dre");
        Barcode code = new Barcode("10008000");
        MedicalDetail med = new MedicalDetail("Aspirina");
        List<MedicalDetail> meds = new ArrayList<MedicalDetail>();
        meds.add(med);
        String patientName = "Juan Patient";
        Date today = new Date();
        Wristband band = new AllergyWristband("Sulfa", patientName, doctor, today, code, meds);

        List<Wristband> bands = new ArrayList<Wristband>();
        bands.add(band);

        Patient patient = new Patient(bands, patientName);
        List<Patient>patients = new ArrayList<Patient>();
        patients.add(patient);
        
        ResearchGroup group = new ResearchGroup(patients);
        System.out.println("Patient name: " + patient.getName());
        System.out.println("Barcode band: " + code.getBarCode());
        System.out.println("Alergys Details: " + band.toString());
        System.out.println("Meds Details: " + med.getMed());
    }
}
