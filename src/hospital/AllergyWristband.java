/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class AllergyWristband extends Wristband{
    private String allergyName;

    public AllergyWristband(String allergyName, String name, Doctor doc, Date dob, Barcode barCode, List<MedicalDetail> medicaments) {
        super(name, doc, dob, barCode, medicaments);
        this.allergyName = allergyName;
    }
    
    @Override public String toString(){
        return allergyName;
    }
    
}
