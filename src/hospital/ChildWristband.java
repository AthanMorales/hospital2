/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class ChildWristband extends Wristband{
    List<Parent> parents;

    public ChildWristband(List<Parent> parents, String name, Doctor doc, Date dob, Barcode barCode, List<MedicalDetail> medicaments) {
        super(name, doc, dob, barCode, medicaments);
        this.parents = parents;
    }
    
    @Override public String toString(){
        return parents.toString();
    }
}
